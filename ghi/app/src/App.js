import Nav from './Nav';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConference from './AttendConference';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
        <BrowserRouter>
          <Nav />
            <Routes>
              <Route path="/" element={<App />} />
              <Route index element={<MainPage />} />
              <Route path="/conferences/new" element={<ConferenceForm />} />
              <Route path="/attendees/new" element={<AttendConference />} />
              <Route path="/locations/new" element={<LocationForm />} />
              <Route path="/presentations/new" element={<PresentationForm />} />
              <Route path="/attendees" element={<AttendeesList attendees={props.attendees} />} />
            </Routes>
        </BrowserRouter>
    </>
  );
}

export default App;
