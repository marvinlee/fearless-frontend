function createCard(name, description, pictureUrl, startDate, endDate, location) {
  return `
    <div class="card shadow mb-5 bg-white rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer text-muted">
        <p>${new Date(startDate).toLocaleDateString()} - ${new Date(endDate).toLocaleDateString()}</p>
      </div>
    </div>
  `;
}


function alert(message){
  var wrapper = document.createElement("div");
  wrapper.innerHTML = '<div class="alert alert-dark" role="alert">'+message+'</div>'
  document.body.appendChild(wrapper);
}


window.addEventListener('DOMContentLoaded', async () => {
  console.log("Up and running");
  const url = 'http://localhost:8000/api/conferences/';
  try {
    const response = await fetch(url);

    if (!response.ok) {
      alert("Reponse not ok");
      throw new Error ("Response not ok");

    } else {
      const data = await response.json();

      let index = 0;
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = details.conference.starts;
          const endDate = details.conference.ends;
          const location = details.conference.location.name;
          const html = createCard(title, description, pictureUrl, startDate, endDate, location);
          // console.log(html);
          const column = document.querySelectorAll(`.col`);
          column[index % 3].innerHTML += html;
          index++;
        }
      }
    }
  } catch (error) {
    console.error("error", error);
    alert("Error");
  }
});
