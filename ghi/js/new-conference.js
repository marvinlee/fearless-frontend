window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/locations/';
  try {
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      const elem = document.getElementById("location");

      for (let location of data.locations) {
        const newOption = document.createElement("option");
        newOption.value = location.id;
        newOption.innerHTML = location.name;
        elem.appendChild(newOption);
      }
    }
  } catch (error) {
    console.error("error, error");
  }

  const formTag = document.getElementById('create-conference-form')
  formTag.addEventListener('submit', async event => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    const conferenceUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newConference = await response.json();
      console.log(newConference);
    }
  })
});
